#! /usr/bin/env python
import toolkit
import sys
import re

#  FUNCTION LIST
#length_string(string_to_count):
#count_in_string(char_to_count, countString):
#valid_seq(seq):
#printerror():
#import_codons(filename):
#GC_Count():
#counting_nucleotides():
#toReverseComplement(dnatemplate):
#reversecomplement():
#dna_to_rna(dnatemplate):
#transcription():
#change_to_uracil(dnaFrag):
#translation():


def print_ERROR():
    print """Error: USAGE: python dnatoolkit.py: option input.fasta output.txt\n\tAvailable options:\n\t\t-g GC%\n\t\t-r reverse complement\n\t\t-s transcription\n\t\t-t translation\n\t\t-c count nucleotides\n"""


if len(sys.argv) < 3 or len(sys.argv) > 5:
    print_ERROR()
    sys.exit()


choice = sys.argv[1]
if choice[0] is not "-":
    print_ERROR()
    sys.exit()
infile = sys.argv[2]
outfile = sys.argv[3]


if choice == "-t":
    if len(sys.argv != 5):
        print_ERROR()
        sys.exit()
    else:
        #ENTER PARAMETERS
        codon_file = sys.argv[1]
        infile = sys.argv[2]
        outfile = sys.argv[3]
        toolkit.translation(codon_file, infile, outfile)

elif choice == "-g":
    if len(sys.argv) != 4:
        print_ERROR()
        sys.exit()
    else:
        toolkit.GC_Count(infile, outfile)
        
elif choice == "-r":
    if len(sys.argv) != 4:
        print_ERROR()
        sys.exit()
    else:
        toolkit.reversecomplement(infile, outfile)

elif choice == "-s":
    if len(sys.argv) != 4:
        print_ERROR()
        sys.exit()
    else:
        toolkit.transcription(infile, outfile) 

elif choice == "-c":
    if len(sys.argv) != 4:
        print_ERROR()
        sys.exit()
    else:
        toolkit.counting_nucleotides(infile, outfile)



