# README #

Read first before running.

### What is this repository for? ###

This is a simple DNA toolkit written for a final project in an introductory Bioinformatics course.  

The program accepts several command line arguments.

The general format is >>python dnatoolkit.py (option) (input FASTA file) (output file)

If the translation option is chosen the command line requires one extra argument, which is the codons file.
Format for Transcription option: >>python dnatoolit.py -t codons.txt (input FASTA file) (output file)


options:
-g  Calculate the percent of G and C nucleotides in a fasta file lin
-r   Outputs a new fast file with the reverse complement strand of the given DNA strand
-s  This option outputs a FASTA file with an mRNA strand transcription of the original DNA strand
-c  This option outputs a file that shows the count of each Nucleotide and percent of nucleotide in the DNA strand
-t   translation

### How do I get set up? ###




### Who do I talk to? ###

Author and owner of code:  Adam Lee alee.byu@gmail.com