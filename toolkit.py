__author__ = 'adamlee'
#
import sys
import re



def length_string(string_to_count):
    length = 0
    for x in string_to_count:
        length += 1
    return length

def count_in_string(char_to_count, countString):
    count = 0
    length_of_countString = length_string(countString) 
    for x in range(0, length_of_countString):
        if countString[x] == char_to_count:
            count += 1
    return count


def valid_seq(seq):
    seq = seq.upper()
    seq = seq.replace(" ","")
    seq = seq.replace("A","")
    seq = seq.replace("C","")
    seq = seq.replace("G","")
    seq = seq.replace("T","")
    if len(seq) != 0:
        return False
    return True



def printerror(fileout):
    fileout.write("ERROR\n")



def import_codons(filename):
    infile = open(filename, "r")
    codons = {}
    while True:
        line = infile.readline().strip()
        if not line:
            break
        else:
            fields = line.split('\t')
            codons[fields[0]] = fields[1]
    infile.close()
    return codons


#NEED TO CHANGE THIS TO ACCEPT A FASTA FILE AS THE INPUT
def GC_Count(filein, fileout):
    infile = open(filein, "r")
    outfile = open(fileout, "w")
    
    outfile.write("ID\tGC%\n")
    while True:
        line = infile.readline().strip()
        if not line:
            break
        if line[0] == '>':
            header = line
            search = r">"
            replace = r""
            header = re.sub(search, replace, header)
            outfile.write(header + "\t")
        else:
            dna = line
            valid = valid_seq(dna)
            if valid == False:
                printerror(outfile)
            elif valid == True:
                dnax = dna.replace(" ","")
                dnalength = len(dnax)
                dnalength = float(dnalength)
                GCcount = dnax.count("G") + dnax.count("g") + dnax.count("C") + dnax.count("c")
                GCpercentdecimal = GCcount / dnalength 
                GCpercent = GCpercentdecimal * 100
                outfile.write("%.2f%%\n" % (GCpercent))


def counting_nucleotides(filein, fileout):

    infile = open(filein, "r")
    outfile = open(fileout, "w")
    outfile.write("ID\tLength\tA(%A)\tT(%T)\tG(%G)\tC(%C)\n")
    seqNumber = 1
    while True:
        line = infile.readline().strip()
        if not line:
            break
        if line[0] == '>':
            header = line
            search = r">"
            replace = r""
            header = re.sub(search, replace, header)
            outfile.write(header + "\t" + "\n")
        else:
            dna = line
            valid = valid_seq(dna)
            
            valid = valid_seq(line)  
            if valid == False:
                outfile.write("ERROR\n")
            elif valid == True:
                dna = dna.replace(" ", "")
                dna = dna.upper()
                length_dna = length_string(dna)
                countA = count_in_string('A', dna)
                countC = count_in_string('C', dna) 
                countG = count_in_string('G', dna) 
                countT = count_in_string('T', dna) 
                length_dna = float(length_dna)
                perA = float(countA/length_dna) *100
                perC = countC/length_dna * 100
                perG = countG/length_dna * 100
                perT = countT/length_dna * 100
                outfile.write("%d\t%d(%.2f%%)\t%d(%.2f%%)\t%d(%.2f%%)\t%d(%.2f%%)\n" % (length_dna, countA, perA, countT, perT, countG, perG, countC, perC)) 

    infile.close()
    outfile.close()
    
   





def toReverseComplement(dnatemplate):
    dnacoding = "z" * len(dnatemplate)
    dnatemplate = dnatemplate.upper()
    dnatemplatelist = list(dnatemplate)
    dnacodinglist = list(dnacoding)
    g = "G"
    c = "C"
    t = "T"
    a = "A"
    for x in range(0, len(dnatemplate)):
        if dnatemplatelist[x] == g:
            dnacodinglist[x] = c
        elif dnatemplatelist[x] == c:
            dnacodinglist[x] = g
        elif dnatemplatelist[x] == t:
            dnacodinglist[x] = a
        elif dnatemplatelist[x] == a:
            dnacodinglist[x] = t
    dnacodinglist
    dnacodinglist.reverse()
    return dnacodinglist


def reversecomplement(filein, fileout):
    """

    """
    infile = open(filein, "r")
    outfile = open(fileout, "w")

    while True:
        line = infile.readline().strip()
        if not line:
            break
        if line[0] == ">":
            header = line
            outfile.write(header + "\n")
        else:
            dna = line
            reverselist = toReverseComplement(dna)
            dnastring = ''.join(dna)
            reverseliststring = ''.join(reverselist)
            outfile.write(reverseliststring+ "\n")



def dna_to_rna(dnatemplate):
    dnatemplate = dnatemplate.upper()
    search = r"T"
    replace = r"U"
    rna = re.sub(search, replace, dnatemplate)
    return rna

def transcription(filein, fileout):
    """

    """

    infile = open(filein, "r")
    outfile = open(fileout, "w")

    while True:
        line = infile.readline().strip()
        if not line:
            break
        if line[0] == ">":
            header = line
            outfile.write(header + "\n")
        else:
            dna = line
            rna = dna_to_rna(dna)
            outfile.write(rna + "\n")




def change_to_uracil(dnaFrag):
    search = r"T"
    replace = r"U"
    dnaT = re.sub(search,replace, dnaFrag)
    #check for replace syntax
    return dnaT



def translation(codonfile, filein, fileout):
    infilefasta = open(filein, "r")
    outfile = open(fileout, "w")
    
    codonDict = import_codons(codonfile)
    while True:
        line = infilefasta.readline().strip()
         
        if not line:
            break
        if line[0] == '>':
            header = line
            outfile.write(header + "\n")
        else:
            line = line.replace(" ","")
            line = line.upper()
            dna = line
            validseq = valid_seq(dna)
            if validseq == False:
                outfile.write("ERROR")
            else:
                dna = change_to_uracil(dna) 
                dnalen = len(dna)
                x = 0
                while x < dnalen:
                    codon = dna[x:x+3]
                    if len(codon) !=3:
                        break    
                    amino_acid = codonDict[codon]
                    if amino_acid == '*':
                       break
                    outfile.write(amino_acid)
                    x += 3
        outfile.write('\n')
    infilefasta.close()
    outfile.close()


