#!/usr/bin/env python
import sys
import re

global countA
global countC
global countG
global countT


def length_string(string_to_count):
    length = 0
    for x in string_to_count:
        length += 1
    return length

def count_in_string(char_to_count, countString):
    count = 0
    length_of_countString = length_string(countString) 
    for x in range(0, length_of_countString):
        if countString[x] == char_to_count:
            count += 1
    return count

#
#def nuc_counts(dna_seq):
#    length = length_string(dna_seq)
#    for x in range(0, length):
#      if   

    

def valid_seq(seq):
    seq = seq.upper()
    seq = seq.replace(" ","")
    seq = seq.replace("A","")
    seq = seq.replace("C","")
    seq = seq.replace("G","")
    seq = seq.replace("T","")
    if len(seq) != 0:
        return False
#    valid = "ACGTacgt "
#    for x in range(0, len(seq)):
#    if seq[x] not in valid:
#        return False
    return True


def main():
    if len(sys.argv) != 3:
        sys.exit("ERROR: Incorrect number of arguments")

    infile = open(sys.argv[1], "r")
    outfile = open(sys.argv[2], "w")
    outfile.write("ID\tLength\tA(%A)\tT(%T)\tG(%G)\tC(%C)\n")
    seqNumber = 1
    while True:
        line = infile.readline().strip()
        if not line:
            break
        if line[0] == '>':
            header = line
            #do a regular expression to get just the name 
            search = r">"
            replace = r""
            header = re.sub(search, replace, header)
            outfile.write(header + "\t")
        else:
            dna = line
            valid = valid_seq(dna)
            
            valid = valid_seq(line)  
            if valid == False:
                outfile.write("ERROR\n")
            elif valid == True:
                dna = dna.replace(" ", "")
                dna = dna.upper()
                length_dna = length_string(dna)
                countA = count_in_string('A', dna)
                countC = count_in_string('C', dna) 
                countG = count_in_string('G', dna) 
                countT = count_in_string('T', dna) 
                length_dna = float(length_dna)
                perA = float(countA/length_dna) *100
                perC = countC/length_dna * 100
                perG = countG/length_dna * 100
                perT = countT/length_dna * 100
                outfile.write("%d\t%d(%.2f%%)\t%d(%.2f%%)\t%d(%.2f%%)\t%d(%.2f%%)\n" % (length_dna, countA, perA, countT, perT, countG, perG, countC, perC)) 

    infile.close()
    outfile.close()
    
    
main()
