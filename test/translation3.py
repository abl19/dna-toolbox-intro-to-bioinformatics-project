#! /usr/bin/env python
import sys
import re

def change_to_uracil(dnaFrag):
    search = r"T"
    replace = r"U"
    dnaT = re.sub(search,replace, dnaFrag)
    #check for replace syntax
    return dnaT

def import_codons(filename):
    infile = open(filename, "r")
    codons = {}
    while True:
        line = infile.readline().strip()
        if not line:
            break
        else:
            fields = line.split('\t')
            codons[fields[0]] = fields[1]
    infile.close()
    return codons

def valid_seq(seq):
    valid = "ACGTacgt "
    for x in range(0, len(seq)):
        if seq[x] not in valid:
            return False

    return True
        


def main():
    infilefasta = open(".:INPUT:.", "r")
    outfile = open(".:OUTPUT:.", "w")
    
    codonDict = import_codons(".:CODON:.")
    while True:
        line = infilefasta.readline().strip()
         
        if not line:
            break
        if line[0] == '>':
            header = line
            outfile.write(header)
        else:
            line = line.replace(" ","")
            line = line.upper()
            dna = line
            validseq = valid_seq(dna)
            if validseq == False:
                outfile.write("ERROR")
            else:
                dna = change_to_uracil(dna) 
                dnalen = len(dna)
                x = 0
                while x < dnalen:
                    codon = dna[x:x+3]
                    if len(codon) !=3:
                        break    
                    amino_acid = codonDict[codon]
                    if amino_acid == '*':
                       break
                    outfile.write(amino_acid)
                    x += 3
        outfile.write('\n')
    infilefasta.close()
    outfile.close()


main()
