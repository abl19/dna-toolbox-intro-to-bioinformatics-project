#! /usr/bin/env python
import sys


#Lab 1 interacting with the user and conditional statements

def validSeq(dnaseq):
    if len(dnaseq) == 0:
        return False;
    dnaseqlength = len(dnaseq)
    dnacompare = "ACGT acgt"
    for x in range(0,dnaseqlength):
        if dnaseq[x] not in dnacompare:
            return False
    return True;


def printerror():
    print "ERROR: Invalid DNA sequence"


def main():
    print "Please enter three DNA sequences when prompted.  An invalid DNA sequence will result in a termination of the program"
    seq1 = raw_input("Sequence 1: ")
    valid = validSeq(seq1)
    if valid == False:
        printerror()
        sys.exit() 
    seq2 = raw_input("Sequence 2: ")
    valid = validSeq(seq2)
    if valid == False:
        printerror()
        sys.exit() 
    seq3 = raw_input("Sequence 3: ")
    valid = validSeq(seq3)
    if valid == False:
        printerror()
        sys.exit()
    seq1nowhite = seq1.replace(" ","")
    seq2nowhite = seq2.replace(" ","")
    seq3nowhite = seq3.replace(" ","")
    seq1length = len(seq1nowhite)
    seq2length = len(seq2nowhite)
    seq3length = len(seq3nowhite)
    print "Sequence 1 length: ",seq1length
    print "Sequence 2 length: ",seq2length
    print "Sequence 3 length: ",seq3length
    fullseq = seq1nowhite + seq2nowhite + seq3nowhite
    fullseqlength = len(fullseq)
    fullseqlength = float(fullseqlength)
    GCcount = fullseq.count("G") + fullseq.count("g") + fullseq.count("C") + fullseq.count("c")
    GCpercentdecimal = GCcount / fullseqlength 
    GCpercent = GCpercentdecimal * 100
    print fullseq
    print "GC Percent: ",GCpercent,"%"
    #print "GC Decimal: ",GCpercentdecimal

main()
