__author__ = 'adamlee'
#! /usr/bin/env python
import sys

def toReverseComplement(dnatemplate):
    dnacoding = "x" * len(dnatemplate)
    dnatemplate = dnatemplate.upper()
    dnatemplatelist = list(dnatemplate)
    dnacodinglist = list(dnacoding)
    g = "G"
    c = "C"
    t = "T"
    a = "A"
    for x in range(0, len(dnatemplate)):
        if dnatemplatelist[x] == g:
            dnacodinglist[x] = c
        elif dnatemplatelist[x] == c:
            dnacodinglist[x] = g
        elif dnatemplatelist[x] == t:
            dnacodinglist[x] = a
        elif dnatemplatelist[x] == a:
            dnacodinglist[x] = t
    dnacodinglist
    dnacodinglist.reverse()
    return dnacodinglist


def main():
    """

    """

    infile = open(".:INPUT:.", "r")
    outfile = open(".:OUTPUT:.", "w")

    while True:
        line = infile.readline().strip()
        if not line:
            break
        if line[0] == ">":
            header = line
            outfile.write(header + "\n")
        else:
            dna = line
            reverselist = toReverseComplement(dna)
            dnastring = ''.join(dna)
            reverseliststring = ''.join(reverselist)
            outfile.write(reverseliststring+ "\n")


main()




