__author__ = 'adamlee'
#! /usr/bin/env python
import sys
import re

def dna_to_rna(dnatemplate):
    dnatemplate = dnatemplate.upper()
    search = r"[T]"
    replace = r"U"
    rna = re.sub(search, replace, dnatemplate)
    return rna

def main():
    """

    """

    infile = open(".:INPUT:.", "r")
    outfile = open(".:OUTPUT:.", "w")

    while True:
        line = infile.readline().strip()
        if not line:
            break
        if line[0] == ">":
            header = line
            outfile.write(header + "\n")
        else:
            dna = line
            rna= dna_to_rna(dna)
            outfile.write(rna + "\n")

#  IT'S ONLY WRITING THE FIRST TWO LINES OF THE FILE!


main()
